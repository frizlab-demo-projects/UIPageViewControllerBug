/*
 * ViewController.swift
 * UIPageViewControllerBug
 *
 * Created by François Lamboley on 02/01/2022.
 */

import UIKit

import XibLoc



class ViewController : UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
	
	@IBOutlet var labelCurrentColor: UILabel!
	private var initialCurrentColorText: String!
	
	var pageViewController: UIPageViewController!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		pageViewController.setViewControllers([ColorViewController(color: .red)], direction: .forward, animated: false, completion: nil)
		
		initialCurrentColorText = labelCurrentColor.text
		updateLabelCurrentColor()
	}
	
	@IBSegueAction
	func embedPageViewController(_ coder: NSCoder) -> UIPageViewController? {
		pageViewController = UIPageViewController(coder: coder)
		pageViewController.dataSource = self
		pageViewController.delegate = self
		return pageViewController
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
		switch (viewController as? ColorViewController)?.color {
			case .red?:   return nil
			case .green?: return ColorViewController(color: .red)
			case .blue?:  return ColorViewController(color: .green)
			default: return nil
		}
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
		switch (viewController as? ColorViewController)?.color {
			case .red?:   return ColorViewController(color: .green)
			case .green?: return ColorViewController(color: .blue)
			case .blue?:  return nil
			default: return nil
		}
	}
	
	func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
		updateLabelCurrentColor()
	}
	
	private func updateLabelCurrentColor() {
		let colorStr: String
		if let vc = pageViewController.viewControllers?.first as? ColorViewController,
			pageViewController.viewControllers?.count == 1
		{
			colorStr = vc.color.rawValue
		} else {
			colorStr = "unknown"
		}
		labelCurrentColor.text = initialCurrentColorText.applyingCommonTokens(simpleReplacement1: colorStr)
		DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
			print(self.pageViewController.viewControllers?.compactMap{ ($0 as? ColorViewController)?.color.rawValue })
		})
	}
	
}


class ColorViewController : UIViewController {
	
	enum Color : String {
		
		case red
		case green
		case blue
		
	}
	
	var color: Color
	
	init(nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil, color: Color = .red) {
		self.color = color
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		updateViewColor()
	}
	
	required init?(coder: NSCoder) {
		self.color = .red
		super.init(coder: coder)
		updateViewColor()
	}
	
	private func updateViewColor() {
		switch color {
			case .red:   view.backgroundColor = .red
			case .green: view.backgroundColor = .green
			case .blue:  view.backgroundColor = .blue
		}
	}
	
}
